/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strepur.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <cobrunet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/09 15:36:32 by cobrunet          #+#    #+#             */
/*   Updated: 2016/05/22 19:12:33 by lsaussy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

char			*ft_strepur(char *str)
{
	while (*str)
	{
		if (!ft_isspace(*str))
			return (str);
		str++;
	}
	return (str);
}

t_header		*init_theader(void)
{
	t_header	*header;
	int			padding;

	if (!(header = malloc(sizeof(*header))))
		put_error(0);
	header->magic = COREWAR_EXEC_MAGIC;
	padding = 4 - (PROG_NAME_LENGTH + 1) % 4;
	ft_bzero(header->prog_name, PROG_NAME_LENGTH + 1 + padding);
	padding = 4 - (COMMENT_LENGTH + 1) % 4;
	ft_bzero(header->comment, COMMENT_LENGTH + 1 + padding);
	return (header);
}
