/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_power.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <cobrunet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 15:01:33 by cobrunet          #+#    #+#             */
/*   Updated: 2016/03/19 14:26:53 by cobrunet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long double		ft_power(long double x, long double y)
{
	long double	res;
	int			neg;

	res = x;
	neg = (y < 0) ? 1 : 0;
	if (neg)
		y *= -1;
	if (!y)
		return (1);
	while (--y > 0)
		res *= x;
	if (neg)
		res = 1 / res;
	return (res);
}
