/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strisspace.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <cobrunet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/09 15:12:27 by cobrunet          #+#    #+#             */
/*   Updated: 2016/05/09 15:14:05 by cobrunet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strisspace(const char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (!(ft_isspace(str[i])))
			return (0);
		i++;
	}
	return (1);
}
