/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <cobrunet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 16:20:31 by cobrunet          #+#    #+#             */
/*   Updated: 2016/03/19 14:33:23 by cobrunet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_wstrlen(wchar_t *chr)
{
	int		res;

	res = 0;
	while (*chr)
	{
		if (*chr <= 0x7F)
		{
			if ((*chr >= 1 && *chr <= 32) || *chr == 127)
				res += 3;
			res += 1;
		}
		else if (*chr <= 0x7FF)
			res += 2;
		else if (*chr <= 0xFFFF)
			res += 3;
		else if (*chr <= 0x10FFFF)
			res += 4;
		chr++;
	}
	return (res);
}
