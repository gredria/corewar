/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <cobrunet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 17:58:32 by cobrunet          #+#    #+#             */
/*   Updated: 2016/05/24 17:58:37 by cobrunet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int			count(char **t)
{
	int i;

	i = 0;
	while (t[i] != NULL)
		i++;
	return (i);
}
