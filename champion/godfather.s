.name "Godfather"
.comment "I'll make him an offer he can't refuse"

sti r1, %:liver, %1 		# put inside the live instruction the num of the player
fork %:loading2
live %0						# just for avoid the dead
fork %:loading3
ldi %:to_copy, %0, r2
ld %10, r3
and %0, %0, r6				# change carry to 1
zjmp %:to_copy

loading2:
	live %0					# just for avoid the dead
	fork %:next_loading2	# this fork is just for wait p1 and p3
	next_loading2:
	ldi %:to_copy, %4, r2
	ld %14, r3
	and %0, %0, r6			# change carry to 1
	zjmp %:to_copy

loading3:
	ldi %:to_copy, %6, r2
	ld %16, r3
	and %0, %0, r6			# change carry to 1
	zjmp %:to_copy

#zone to copy
to_copy:
	sti r2, r3, r4
	liver: live %0
