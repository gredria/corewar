/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/10 13:16:12 by cobrunet          #+#    #+#             */
/*   Updated: 2016/03/19 14:33:00 by cobrunet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_swap(int *a, int *b)
{
	int tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

void	ft_swap2(int *xdep, int *xfin, int *ydep, int *yfin)
{
	ft_swap(xdep, xfin);
	ft_swap(ydep, yfin);
}
