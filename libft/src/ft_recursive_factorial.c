/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 09:40:24 by cobrunet          #+#    #+#             */
/*   Updated: 2016/03/19 14:27:53 by cobrunet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_recursive_factorial(int nb)
{
	int fact;

	fact = 0;
	if (nb == 1 || nb == 0)
	{
		fact = 1;
		return (fact);
	}
	else if (nb < 0 || nb > 12)
		return (0);
	else
	{
		fact = nb * ft_recursive_factorial(nb - 1);
		return (fact);
	}
}
