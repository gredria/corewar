/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <cobrunet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/12 18:01:23 by cobrunet          #+#    #+#             */
/*   Updated: 2016/03/19 14:20:44 by cobrunet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_dlist			*ft_dlst_push_back(t_dlist *list, t_node *new)
{
	if (list != NULL)
	{
		if (list->last == NULL)
		{
			new->prev = NULL;
			list->first = new;
			list->last = new;
		}
		else
		{
			list->last->next = new;
			new->prev = list->last;
			list->last = new;
		}
		list->lenght++;
	}
	return (list);
}
