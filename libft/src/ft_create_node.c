/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cobrunet <cobrunet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/12 18:01:23 by cobrunet          #+#    #+#             */
/*   Updated: 2016/03/19 14:20:37 by cobrunet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_node			*ft_create_node(void)
{
	t_node	*new;
	int		i;

	i = 0;
	new = malloc(sizeof(*new));
	if (new != NULL)
	{
		while (i < 4)
		{
			new->pos[i].x = 0;
			new->pos[i].y = 0;
			i++;
		}
		new->next = NULL;
	}
	return (new);
}
